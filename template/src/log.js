/*
 * @Author: liyonglong 
 * @Date: 2019-10-18 22:35:43 
 * @Last Modified by: liyonglong
 * @Last Modified time: 2019-10-18 23:59:03
 */
const path = require('path')
const log4js = require('log4js')

const logPath = path.join(__dirname, '../logs/')

log4js.configure({
  appenders: {
    // stdout: { type: 'stdout' }, //声明此属性可在控制台上打印信息
    stdout: { type: 'console' }, //声明此属性可在控制台上打印信息
    action: {
      type: 'dateFile',
      filename: `${logPath}/logs/`,
      pattern: 'yyyyMMdd-action.log',
      alwaysIncludePattern: true
    },
    err: {
      type: 'dateFile',
      filename: `${logPath}/logs/`,
      pattern: 'yyyyMMdd-error.log',
      alwaysIncludePattern: true
    }
  },
  categories: {
    default: { appenders: ['stdout', 'action'], level: 'debug' },
    error: { appenders: ['stdout', 'err'], level: 'error' }
  }
})

let actionLog = log4js.getLogger('action')
let errorLog = log4js.getLogger('error')

module.exports = {
  actionLog,
  errorLog
}
