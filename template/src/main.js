const Crawler = require('crawler')
const _ = require('lodash')
const fs = require('fs')
const path = require('path')
const { actionLog, errorLog } = require('./log.js')

class ImagesDownload {
  // { sourcePath, maxConnections = 30, rateLimit = 200, host, prefix = 'crawler', header } = {}
  constructor(
    opts = {},
    { sourcePath = '', host, header = {}, prefix = 'crawler' } = {}
  ) {
    actionLog.info('程序启动,传入参数:' + JSON.stringify(opts))
    actionLog.info(
      '自定义参数 sourcePath: ' +
        sourcePath +
        ' host:' +
        host +
        ' header:' +
        JSON.stringify(header) +
        ' prefix:' +
        prefix
    )
    // 实例
    this.crawler = new Crawler({
      ...opts,
      callback: this.defaultResponseLogic.bind(this)
    })
    this.host = host
    const fullPath = sourcePath || path.resolve(__dirname, '../source')

    // 检测路径是否存在，不存在则创建
    this.dirCheck(fullPath)
    this.sourcePath = fullPath

    this.header = header
    this.prefix = prefix
    this.successCount = 0
    this.errorCount = 0
  }

  /**
   * 检测路径文件夹是否存在，不存在则创建
   *
   * @param {*} pathStr
   * @memberof ImagesDownload
   */
  dirCheck(pathStr) {
    const result = fs.existsSync(pathStr)
    if (!result) {
      fs.mkdirSync(pathStr)
    }
  }

  /**
   * 全局回调函数，通过cb 在去call 不同的方法
   *
   * @param {*} err
   * @param {*} res
   * @param {*} done
   * @returns
   * @memberof ImagesDownload
   */
  defaultResponseLogic(err, res, done) {
    const reqPath = _.get(res, 'request.path', 'unknowRequestPath')
    const { statusCode = 'unknowStatusCode' } = res

    let txt = `[ defaultResponseLogic ]  - [ ${reqPath} ] - error`

    if (err) {
      errorLog.error(txt)
      return done()
    }

    if (`${statusCode}` !== '200') {
      txt = `[response Code ] - :${reqPath} : ${statusCode} `
      errorLog.error(txt)
      return done()
    }

    const cb = res.options.cb || 'listResponseLogic'
    if (res && cb) {
      this[cb].call(this, res, done)
    }
  }

  /**
   *  列表解析
   *
   * @param {*} res
   * @param {*} done
   * @returns
   * @memberof ImagesDownload
   */
  listResponseLogic(res, done) {
    const self = this
    const $ = res.$
    // const list = $('#imglist .imgrow>a>img').attr('src')
    const list = $('.egeli_pic_m  .egeli_pic_li dd a')
    actionLog.debug(`[step1] 进入 listParse`)
    const datailList = []
    if (!list || list.length <= 0) {
      errorLog.fatal('没有list')
      return done()
    }
    list.each((k, v) => {
      const _path = _.get(v, ['attribs', 'href'])
      if (_path) {
        datailList.push({
          url: `${self.host}${_path}`,
          headers: this.header,
          cb: 'datailParse'
        })
      }
    })
    this.crawler.queue(datailList)
    return done()
  }

  /**
   *  获取图片路径下载 并且 下一页
   *
   * @param {*} res
   * @param {*} done
   * @memberof ImagesDownload
   */
  datailParse(res, done) {
    if (res) {
      actionLog.debug(`[step2] 进入 datailParse`)
      const $ = res.$
      let imgPath = $('.arc_main_pic img').attr('src')
      if (imgPath) {
        imgPath = imgPath.replace(/^\/\//, 'https://')
        actionLog.info(`[join] ${imgPath} queue`)
        this.crawler.queue({
          encoding: null,
          url: imgPath,
          jQuery: false,
          filename: $('#img').attr('alt'),
          headers: this.header,
          cb: 'downloadImgCb'
        })
      } else {
        errorLog.error(`datailParse中没有找到图片`)
      }
    }
    done()
  }

  /**
   * 添加url进去队列
   *
   * @param {*} urlQueue
   * @memberof ImagesDownload
   */
  start(urlQueue) {
    actionLog.info(
      '添加url进入队列:' + _.isString(urlQueue)
        ? urlQueue
        : JSON.stringify(urlQueue)
    )
    this.crawler.queue(urlQueue)
  }

  /**
   *
   * 下载图片
   * @param {*} res
   * @param {*} done
   * @returns
   */
  downloadImgCb(res, done) {
    const body = res.body

    const name =
      this.prefix + (res.options.filename || this.successCount) + '.jpg'

    if (_.isBuffer(body)) {
      actionLog.debug(`[downloadImgCb] - start`)
      try {
        const imgLocalFile = `${this.sourcePath}/${name}`
        const file = fs.createWriteStream(imgLocalFile)
        file.end(body)
        this.successCount += 1
        // redis 自增
        actionLog.info(
          `${name} -- download -- success 成功:${this.successCount} \n`
        )
      } catch (error) {
        this.errorCount += 1
        errorLog.error(`download error ::error:${error.message}`)
        errorLog.error(
          `download error 失败${this.errorCount} , 成功:${this.successCount}`
        )
      }
    } else {
      errorLog.error(`response not a Buffer`)
    }
    done()
  }
}

module.exports = ImagesDownload
