const ImagesDownload = require('./src/main.js')

const c = new ImagesDownload(
  {
    // 工作线程池的大小
    maxConnections: 5,
    // 跳过已经爬取的URI
    skipDuplicates: true
  },
  {
    prefix: 'ly-crawler',
    host: 'https://sc.enterdesk.com/',
    // 资源存储
    // sourcePath: path.join(__dirname, './assets'),

    // 自定义参数
    header: {
      'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0',
      Referer: 'https://sc.enterdesk.com/'
    }
  }
)

c.start('https://sc.enterdesk.com/')
